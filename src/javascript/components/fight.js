import { controls } from '../../constants/controls';
import { randomNumber } from '../helpers/randomNumber';

export async function fight(firstFighter, secondFighter) {
  const playerOne = {
    ...firstFighter,
    isBlocked: false,
  };
  const playerTwo = {
    ...secondFighter,
    isBlocked: false,
  };

  const leftFighterIndicator = document.querySelector('#left-fighter-indicator');
  const rightFighterIndicator = document.querySelector('#right-fighter-indicator');

  
  let pressedKeys = [];

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    window.addEventListener('keypress', (e) => {
      let damage = 0;
      let damagePercent;
      let newPercentage;


      
      pressedKeys.push(e.code);

      console.log(pressedKeys);

      const isPlayerOneCritical = arraysEqual(pressedKeys.sort(), controls.PlayerOneCriticalHitCombination.sort());
      const isPlayerTwoCritical = arraysEqual(pressedKeys.sort(), controls.PlayerTwoCriticalHitCombination.sort());

      if(isPlayerOneCritical) {
        console.log('pl1 critical');
        damage = 2 * playerOne.attack;
        damagePercent = getDamagePercent(damage, playerTwo.health);

        newPercentage = 100 - damagePercent;
        rightFighterIndicator.style.width = `${newPercentage}%`;
        // console.log(newPercentage);
  
        playerTwo.health = playerTwo.health - damage;

        if(playerOne.health <= 0) {
          return resolve(1);
        }

        if(playerTwo.health <= 0) {
          return resolve(0);
        }
        pressedKeys = [];
      }
      else if(isPlayerTwoCritical) {
        console.log('pl2 critical');

        damage = 2 * playerTwo.attack;
        damagePercent = getDamagePercent(damage, playerOne.health);

        newPercentage = 100 - damagePercent;
        leftFighterIndicator.style.width = `${newPercentage}%`;
        // console.log(newPercentage);
  
        playerOne.health = playerOne.health - damage;

        if(playerOne.health <= 0) {
          return resolve(1);
        }

        if(playerTwo.health <= 0) {
          return resolve(0);
        }
        pressedKeys = [];
      }

      switch(e.code) {
        case controls.PlayerOneAttack:
          if(!playerOne.isBlocked) {
            damage = getDamage(playerOne, playerTwo);

            damagePercent = getDamagePercent(damage, playerTwo.health);

            newPercentage = 100 - damagePercent;
            rightFighterIndicator.style.width = `${newPercentage}%`;
            playerTwo.health = playerTwo.health - damage;

            if(playerOne.health <= 0) {
              return resolve(1);
            }

            if(playerTwo.health <= 0) {
              return resolve(0);
            }
          }
          
          break;
  
        case controls.PlayerTwoAttack:
          if(!playerTwo.isBlocked) {
            damage = getDamage(playerOne, playerTwo);
            damagePercent = getDamagePercent(damage, playerOne.health);
            playerOne.health = playerOne.health - damage;
  
            newPercentage = 100 - damagePercent;
            
  
            leftFighterIndicator.style.width = `${newPercentage}%`;
            
            if(playerOne.health <= 0) {
              return resolve(1);
            }
  
            if(playerTwo.health <= 0) {
              return resolve(0);
            }
          }
        
          break;
  
        case controls.PlayerOneBlock:
          playerOne.isBlocked = true;
          break;
  
        case controls.PlayerTwoBlock:
          playerTwo.isBlocked = true;
          break;

        default:
          pressedKeys = [];
          break;
      }
    })
  
    window.addEventListener('keyup', (e) => {
      switch(e.code) {
        case controls.PlayerOneBlock:
          playerOne.isBlocked = false;
          break;
        case controls.PlayerTwoBlock:
          playerTwo.isBlocked = false;
          break;
      }
    })  
  });
}

function arraysEqual(a, b) {
  return Array.isArray(a) &&
    Array.isArray(b) &&
    a.length === b.length &&
    a.every((val, index) => val === b[index]);
}

function getDamagePercent(damage, health) {
  const result = damage / health * 100;

  return result;
}

export function getDamage(attacker, defender) {
  let damage = 0;
  
  const hitPower = getHitPower(attacker);
  
  if(defender.isBlocked) {
    const blockPower = getBlockPower(defender);
    const difference = hitPower - blockPower;
    damage = difference > 0 ? difference : 0;
  }

  else {
    damage = hitPower;
  }
  
  console.log(damage);

  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = randomNumber(1, 2); 
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = randomNumber(1, 2);
  const power = fighter.defense * dodgeChance;

  return power;
}