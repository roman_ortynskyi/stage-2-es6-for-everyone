import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  if(fighter) {
    const fighterElement = createElement({
      tagName: 'div',
      className: `fighter-preview___root ${positionClassName}`,
    });
  
    const fighterImage = createFighterImage(fighter);

    const fighterNameElement = createElement({
      tagName: 'h2',
    });

    fighterNameElement.innerText = fighter.name;

    const fighterAttackElement = createElement({
      tagName: 'span',
    });

    fighterAttackElement.innerText = `Attack: ${fighter.attack}`;

    const fighterDefenseElement = createElement({
      tagName: 'span',
    });

    fighterDefenseElement.innerText = `Defense: ${fighter.defense}`;

    const fighterHealthElement = createElement({
      tagName: 'span',
    });

    fighterHealthElement.innerText = `Health: ${fighter.health}`;
  
    fighterElement.append(fighterNameElement);

    fighterElement.append(fighterAttackElement);
    fighterElement.append(fighterDefenseElement);
    fighterElement.append(fighterHealthElement);

    fighterElement.append(fighterImage);
  
    return fighterElement;
  }
  else return '';
  
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
