import { createElement } from "../../helpers/domHelper";
import { showModal } from "./modal";
import { fighterService } from '../../services/fightersService';
import { createFighters } from '../../components/fightersView';

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({
    title: `${fighter.name} wins!`,
    bodyElement: createElement({
      tagName: 'img',
      attributes: {
        src: fighter.source,
      },
    }),
    onClose: async () => {
      const root = document.querySelector('#root');

      root.innerHTML = '';

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);
      
      root.appendChild(fightersElement);
    }
  }); 
}
